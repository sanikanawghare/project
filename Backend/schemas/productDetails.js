var mongoose = require("mongoose")
var productDetails = new mongoose.Schema({
    productName: String,
    // productOwner: String,
    productCost:Number,
    shippingCost:Number,
    // shippingAddress:String,
    // phoneNumber:Number,
    description:String,
    status:String,
    // date:String,
    imagePath: String
    }); 
    module.exports= mongoose.model("productDetails", productDetails);