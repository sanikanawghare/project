var mongoose = require("mongoose")
var orderSummaryDetails = new mongoose.Schema({
    cart:{
        type:Array,
        ref:'productDetails'
    },
    shipping:Array,
    userId:String,
    date:String
 }); 
module.exports= mongoose.model("orderSummaryDetails", orderSummaryDetails);