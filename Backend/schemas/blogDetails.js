var mongoose = require("mongoose")
var blogDetails = new mongoose.Schema({
    blogTitle: String,
    des:String,
    blogImage: String,
    date:String,
    ratings:Number
 }); 
module.exports= mongoose.model("blogDetails", blogDetails);