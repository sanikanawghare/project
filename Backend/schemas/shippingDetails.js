var mongoose = require("mongoose")
var shippingDetails = new mongoose.Schema({
    fullName: String,
    email:String,
    contact: Number,
    addressLine1:String,
    addressLine2:String,
    city:String,
    state:String,
    country:String,
    zipcode:Number,
    userId:String,
    totalAmount:Number
 }); 
module.exports= mongoose.model("shippingDetails", shippingDetails);