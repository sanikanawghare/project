var mongoose = require("mongoose")
var commmentsDetails = new mongoose.Schema({
    userId:{
        type:String,
        ref:'userDetails'
    },
    blogId:{
        type:String,
        ref:'blogDetails'
    },
    comments:String,
    date:String,
    rating:Number
 }); 
module.exports= mongoose.model("commmentsDetails", commmentsDetails);