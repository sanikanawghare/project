const mongoose = require("mongoose")

const userDetails = new mongoose.Schema({
    fullName: String,
    email:{type:String, unique: true},
    username:{type:String, unique: true},
    password:String,
    role:String,
    isDeactive:String,
    });


module.exports= mongoose.model("userDetails", userDetails);
