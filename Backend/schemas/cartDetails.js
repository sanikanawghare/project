var mongoose = require("mongoose")
var cartDetails = new mongoose.Schema({
    userId:{
        type:String,
        ref:'userDetails'
    },
    productId:{
        type:String,
        ref:'productDetails'
    },
    quantity:{
        type:Number,
        default:1
    },
    date:{
        type:String,
        default:Date.now()
    },
    productRate:Number
 }); 
module.exports= mongoose.model("cartDetails", cartDetails);