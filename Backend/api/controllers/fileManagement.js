var imageSchema = require("../../schemas/imageDetails");
var fs = require('fs');
var img = new imageSchema();

module.exports = {
    imgUp: imgUp,
};

timeStamp = Date.now();

function imgUp(req, res) {
    imgOriginalName = req.files.file[0].originalname;
    path = './ImageUploads/' + timeStamp + "_" + imgOriginalName;
    img.imagePath = path;
    console.log("Image",req.files)
    fs.writeFile(path, (req.files.file[0].buffer), function (err) {
        if (err) throw err;
        img.save(img, function (err, data) {
            if(err) throw err;
            res.json(data);
        });
    })
}