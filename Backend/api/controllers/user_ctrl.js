'use strict';
var util = require('util');
var user = require('../../schemas/userDetails');
var comments = require('../../schemas/commentsDetails');
var cart = require('../../schemas/cartDetails');
const bcrypt = require('bcrypt-nodejs');
var shipping = require('../../schemas/shippingDetails');
var summary = require('../../schemas/orderSummaryDetails');
var waterfall = require('async-waterfall');

// const keyPublishable = process.env.pk_test_OaKJ06YG3DM3GUb2TfvuGQ87;
const keySecret = process.env.sk_test_3BkdsfENe7x9hNBW76FQTwge;

const stripe = require("stripe")(keySecret);

let jwt = require('jsonwebtoken');
module.exports = {
    addUser: addUser,
    login: login,
    addComments: addComments,
    getComments: getComments,
    addToCart: addToCart,
    listCart: listCart,
    deleteItem: deleteItem,
    updateQuantity: updateQuantity,
    addShippingDetails: addShippingDetails,
    payment: payment,
    orderSummary:orderSummary,
    listOrderSummary:listOrderSummary,
    flushCart:flushCart
};

function addUser(req, res) {
    // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
    console.log(" here", req.body);
    var record = new user();
    record.fullName = req.body.fname;
    record.email = req.body.email;
    record.username = req.body.uName;
    record.password = req.body.psword //bcrypt.hashSync(req.body.psword, bcrypt.genSaltSync(8), null);
    record.role = 'user';
    record.isDeactive = 'active'
    record.save(function (err, res1) {
        console.log(" Resp =", err, res1);
        if (err) {
            res.json({
                data: err,
                message: 'user already exists',
            });
        } else {
            res.json({
                message: "Sign up Sucess"
            });
        }
    });
}

function login(req, res) {
    let data = {
        username: req.body.uName,
        password: req.body.psword
    };
    user.findOne(data).lean().exec(function (err, user) {
        console.log("i am here", err, user)
        if (err) {
            return res.json({ error: true });
        }
        else if (!user || user.isDeactive=='deactive') {
            return res.status(404).json({ 'message': 'User not found!' });
        }
        else {
            let token = jwt.sign(user, "sanika", {
                expiresIn: 60 * 60 * 8 * 1
            });
            res.json({
                message: "hfdhsf..........................",
                code:200,
                token: token,
                user: user
            });
        }
    })
}

function addComments(req, res) {
    console.log(" here", req.body);
    var record = new comments();
    record.userId = req.body.userid;
    record.blogId = req.body.blogid;
    record.date = Date.now();
    record.comments = req.body.comment;
    record.rating = req.body.rating;
    record.save(function (err, res1) {
        console.log(" Resp =", err, res1);
        if (err) {
            res.json({
                data: err
            });
        } else {
            res.json({
                message: " Successfully added comment"
            });
        }
    });
}

function getComments(req, res) {
    var blogId = req.swagger.params.id.value
    comments.find({ blogId }).sort({ date: 'descending' }).populate({
        path: 'userId',
        model: 'userDetails'
    }).populate({
        path: 'blogId',
        model: 'blogDetails'
    }).exec(function (err, data) {
        if (err) {
            console.log(err)
        } else {
            res.json(data)
        }
    })
}

function addToCart(req, res) {
    var record = new cart();
    record.userId = req.body.userid;
    record.productId = req.body.productid;
    record.quantity = 1;
    record.productRate = req.body.product_cost;

    cart.findOne({ productId: req.body.productid, userId: req.body.userid }, function (err, result) {
        if (err) {
            console.log(err);
        } else if (result) {
            console.log(result);
            var quant = result.quantity + 1

            var totalRate = quant * record.productRate;
            cart.findOneAndUpdate({ productId: req.body.productid }, { $set: { productRate: totalRate, quantity: quant } }, function (err, result) {
                if (err) {
                    console.log(err);
                } else {
                    res.json({
                        code: 200,
                        message: "success",
                        data: result
                    })
                }
            })
        }
        else {
            record.save(function (err, response) {
                if (err) {
                    res.json({
                        code: 404,
                        message: 'product not Added',
                        // data:response
                    })
                } else {
                    res.json({
                        code: 200,
                        data: response
                    })

                }
            })

        }
    })

}


function listCart(req, res) {
    cart.find({ userId: req.swagger.params.userid.value }).populate({
        path: 'productId',
        model: 'productDetails'
    }).exec(function (err, data) {
        if (err) {
            console.log(err)
        } else {
            // var totalAmt = 0;
            // for (var i = 0; i < data.length; i++) {
            //     totalAmt = totalAmt + data[i].productRate
            // }
            res.json(data);
        }
    })
}

function deleteItem(req, res) {
    console.log("inside delete", req.swagger.params.id.value)
    cart.findByIdAndRemove({ _id: req.swagger.params.id.value }, function (err, data) {
        if (err) {
            console.log(err)
        } else {
            console.log("Deleted Successfully");
            res.json(data);
        }
    })
}

function updateQuantity(req, res) {
    var data = {
        quantity: req.body.quantity,
        productRate: req.body.rate
    }
    var condition = { productId: req.body.productid, userId: req.body.userid }
    console.log("data=======", data);
    console.log("ids=======", condition);

    var totalPrice = data.quantity * data.productRate;
    cart.update(condition, { $set: { productRate: totalPrice, quantity: data.quantity } }, function (err, data1) {
        console.log(" resp====", err, data1);
        if (err) throw err;
        // data1.save();
        console.log("Updated Successfully", data1);
        console.log("Updated data", data1);
        res.send(data1);
    });
}

function addShippingDetails(req, res) {
    var record = new shipping();
    record.fullName = req.body.fullName;
    record.email = req.body.email;
    record.contact = req.body.contact;
    record.addressLine1 = req.body.addressLine1;
    record.addressLine2 = req.body.addressLine2;
    record.city = req.body.city;
    record.state = req.body.state;
    record.country = req.body.country;
    record.zipcode = req.body.zipcode;
    record.userId = req.body.userId;
    record.totalAmount = req.body.totalAmount;

    shipping.findOne({userId:record.userId},function(err,result){
        if(err){
            console.log(err)
        }else if(result){
            shipping.findOneAndUpdate({userId:record.userId},{ $set: 
            { 
                fullName:record.fullName,
                email:record.email,
                contact:record.contact,
                addressLine1:record.addressLine1,
                addressLine2:record.addressLine2,
                city:record.city,
                state:record.state,
                country:record.country,
                zipcode:record.zipcode,
                userId:record.userId,
                totalAmount:record.totalAmount,

            }},function(err,data){
                if(err){
                    console.log(err)
                }else{
                    res.json({
                        message:"Successfully added shipping details"
                    })
                }
            })
        }else{
            record.save(function (err, data) {
                if (err) {
                    console.log(err);
                } else {
                    res.json({
                        message: "Successfully added shipping details"
                    })
                }
            })
        }
    })

}

function payment(req, res) {
    var stripe = require("stripe")("sk_test_3BkdsfENe7x9hNBW76FQTwge");
    const token = req.body.stripeToken;
    const amount = req.body.amount;
    const payableAmount = amount * 100;
    // const userid = req.body.userId;
    const charge = stripe.charges.create({
        amount: payableAmount,
        currency: 'usd',
        description: 'Example charge',
        source: token,
    });
    // console.log("charge==========",charge);
    var data = {
        message: "Payment Successful",
        charge: charge
    }
    res.json(data)
}

function orderSummary(req,res){
    var userid=req.body.userid;
    var order={};
    waterfall([
        function(callback){
            cart.find({userId:userid},function(err,data){
                if(err){
                    console.log(err)
                }else if(data){
                   console.log(data);
                    order.cart=data;
                    console.log("cart data=========",order);
                   callback(null,data)
                }else{
                    callback(err)
                }
            })
            shipping.find({userId:userid},function(err,data){
                if(err){
                    console.log(err)
                }else if(data){
                    console.log("shipping data=====",data);
                    order.shipping=data;
                    console.log("order.shipping========",order.shipping);

                    var record = new summary();

                    record.cart=order.cart;
                    record.shipping=order.shipping;
                    record.userId=userid;
                    record.date=Date.now();

                    record.save(function(err,data){
                        if(err){
                            console.log(err);
                        }else{
                            callback(data)
                        }
                    })

                   
                }else{
                    callback(err)
                }
            })
        }
        
    ]),
    function(err,data){
        if(err){
            console.log(err)
        }else{
            res.json(data)
        }
    }
}

function listOrderSummary(req, res) {
    summary.find({userId : req.swagger.params.userid.value}).sort({ date: 'descending' }).populate({
        path: 'cart.productId',
        model: 'productDetails'
    }).exec(function (err, data) {
        if (err) {
            console.log(err)
        } else {
            res.json(data);
        }
    })
}

function flushCart(req,res){
    cart.remove({userId:req.swagger.params.id.value},function(err,data){
        if(err){
            console.log(err)
        }else{
            console.log("Deleted Successfully");
            res.json(data);
        }
    })
}



