'use strict';
var express = require('express');
var path= require('path');
var SwaggerExpress = require('swagger-express-mw');
var utils=require('./lib/util')
var app = express();
module.exports = app; // for testing

var db=require('./db');

var config = {
  appRoot: __dirname // required config
};

app.use(express.static(path.join(__dirname, 'public')));


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, authorization");
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE,OPTIONS');
  next();
});


app.use('/api/*', function(req, res, next) {
  var freeAuthPath = [
    '/api/addUser',
    '/api/login',
    '/api/payment'
  ];
  var available = false;
  for (var i = 0; i < freeAuthPath.length; i++) {
  if (freeAuthPath[i] == req.baseUrl) {
  available = true;
  break;
  }
  }
  if (!available) {
  utils.ensureAuthorized(req, res, next);
  } else {
  next();
  }
  });

SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }
app.use(swaggerExpress.runner.swaggerTools.swaggerUi());
  // install middleware
  swaggerExpress.register(app);

  var port = process.env.PORT || 10010;
  app.listen(port);

  if (swaggerExpress.runner.swagger.paths['/hello']) {
    console.log('try this:\ncurl http://127.0.0.1:' + port + '/hello?name=Scott');
  }
});
