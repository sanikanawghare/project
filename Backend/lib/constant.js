
const messages = {
    "saveDataError": "Could not save the Form Data.",
    "saveData": "Ok.",
    "error": "Request could not processed. Please try again.",
    "success": "Form has been submitted",
    "EventAlreadyExist": "The Event name you entered is already exist",
    "ApplicationNotExist": "The Requested Application Url does not exist.",
    "logIn": "You have successfully Logged In ",
    "LogOut": "You have successfully Logged Out",
    "dataNotFound": 'Requested Data not found.',
    "commonError": 'Request could not be processed.',
    "emailPass": "email or password not found !",
    "InactiveAccount": "Your account is Inactive. !",
    "invalidUser": "Invalid user or password !",
    "forgotSuccess": 'Please check Link send to your mail to reset Password',
    "resetSuccess": 'Password has been updated successfully',
    "apiKey": 'Request Api key added successfully',
    "apiKeyExist": 'Request Api key already exist.',
    "apiKeyList": 'Api key List fetched successfully',
    "apiKeyDeleted": 'Api key already deleted.',
    "apiKeySuccessAdd": 'Requested api key deleted successfully',
    "apiKeyUpdate": 'Api Key updated successfully',
    "apiKeyNotFound": 'Api Key not found.',
    "endpointExist": 'Endpoint already exist.',
    "endpointAdded": 'Endpoint added successfully',
    "endpointUpdated": 'Endpoint updated successfully',
    "endpointNotFound": 'Endpoint data not found.',
    "endpointList": 'Endpoint List fetched successfully',
    "endpointAllReadyDeleted": 'Endpoint already deleted.',
    "endpointDelete": 'Endpoint deleted successfully',
    "endpointDetails": 'Endpoint detail fetched successfully',
    "groupExist": 'Group already exist.',
    "addGroup": 'Group added successfully',
    "updateGroup": 'Group updated successfully',
    "groupList": 'Group List fetched successfully',
    "groupDeleted": 'Group already deleted.',
    "groupDeletedSuccess": 'Group deleted successfully',
    "groupDetails": 'group detail fetched successfully',
    "appExist": 'Request Application already exist.',
    "appAdded": 'Request Application added successfully',
    "updateApp": 'Application  updated successfully',
    "appList": 'Application List fetched successfully',
    "AppDeleted": 'Application already deleted.',
    "AppDeletedSuccess": 'Requested Application url deleted successfully',
    "appDetails": 'Requested Application detail fetched successfully',
    "regionExist": 'Region already exist.',
    "addRegion": 'Region added successfully',
    "updateRegion": 'Region updated successfully',
    "listRegion": 'region List fetched successfully',
    "regionDeleted": 'Region already deleted.',
    "regionDeletedSuccess": 'Region deleted successfully',
    "regionDetails": 'region detail fetched successfully'
}
const validationMessages = {

    "KeyValueError": "Bad Request ! Key  or Value is missing."
}
const connectionMessages = {
    "connected": "Database connected Successfully.",
    "connectionErr": "Failed to connect to Data base."
}

const statusCode = {
    "success": 200,
    "error": 401,
    "warning": 404,
    "failed": 1002,
    "internalError": 1004,
    "failedConnection": 500,
    "okList": 201
}
const webToken = {
    "privateKey": "secret123"
}
const baseUrl = 'http://localhost:4200';
// const baseUrl = 'http://52.34.207.5:5136';

const obj = {
    messages: messages,
    validationMessages: validationMessages,
    statusCode: statusCode,
    connectionMessages: connectionMessages,
    webToken: webToken,
    baseUrl: baseUrl
};
module.exports = obj;