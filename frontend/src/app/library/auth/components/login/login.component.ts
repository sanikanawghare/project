import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MessageService } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  lForm:FormGroup;
  constructor(private messageService: MessageService, private toastr: ToastrService,private fb:FormBuilder,private lUp:AuthService,private router:Router) { }

  ngOnInit() {
    this.lForm=new FormGroup({
      "uName":new FormControl(''),
      "psword":new FormControl(''),
    })
    this.lForm= this.fb.group({  
      uName:['',Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])],   
      psword:['',Validators.required]  
    })
  }
  onLogin(){
    console.log(this.lForm.value);
    this.lUp.login(this.lForm.value)
    .subscribe((res:any)=>{
      console.log("res===========",res);
      // res=res.data;\
      console.log(" res.user==",res.user)
      var token=res.token;
      localStorage.setItem("token",token);
      localStorage.setItem("username",res.user.username);
      localStorage.setItem("_id",res.user._id); 
      localStorage.setItem("role",res.user.role);     
      console.log("Tokennnnnnnnnnnn",res);
      if(localStorage.getItem(token)!="" && res.user.role=='user' && res.user.isDeactive=='active'){
        this.toastr.success('Successfully Logged In');

        this.router.navigate(['/authentication/success/user/perfumes']);
      }else  if(localStorage.getItem(token)!="" && res.user.role=='admin' && res.user.isDeactive=='active'){
        this.toastr.success('Successfully Logged In');        

        this.router.navigate(['/authentication/success/admin']);

      }
      else if(localStorage.getItem(token)=="" || res.status==404){
        this.toastr.error('Oops! Please try again');
        this.router.navigate(['/authentication/login']);
      }
    })
  }

}
