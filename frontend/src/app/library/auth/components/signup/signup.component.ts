import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  sForm:FormGroup;
  constructor(private toastr: ToastrService,private fb:FormBuilder,private sUp:AuthService,private router:Router) { }

  ngOnInit() {
    this.sForm=new FormGroup({
      "fname":new FormControl(''),
      "uName":new FormControl(''),
      "email":new FormControl(''),
      "psword":new FormControl(''),
    
    })
    this.sForm= this.fb.group({  
      fname:['',Validators.compose([Validators.required,Validators.maxLength(30),Validators.minLength(3)])],
      uName:['',Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])],   
      email:['',Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],   
      psword:['',Validators.required] 
    })
  }
  onSignUp(){
    console.log(" Me ithe ahe");

    this.sUp.signUp(this.sForm.value)
    .subscribe((res:any)=>{
      alert(res.message);
      // res=res.data;
      console.log("Data resp -------",res);
      this.toastr.success('Successfully Signed Up');
      this.router.navigate(['/authentication/login']);
    })
  }
}
