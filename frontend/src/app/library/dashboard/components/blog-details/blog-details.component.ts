import { Component, OnInit } from '@angular/core';
import { CommentsService } from '../../../../services/comments/comments.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AddBlogService } from '../../../../services/add-blog/add-blog.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.css']
})
export class BlogDetailsComponent implements OnInit {
  commentList: any;
  bList=new Array;
  blogList = new Array;
  userList = new Array;
  userid;
  val: number;

  constructor(private toastr: ToastrService,private addComments: CommentsService,private router:Router,private getBListId:AddBlogService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getBlogListId();
    this.userid = (localStorage.getItem("_id"));
  }
  getBlogListId(){
    this.route.params.subscribe(params=>{
      this.getBListId.getBlogListId(params['id']).subscribe((res:any)=>{
        this.bList.push(res);
        console.log("blog list with is",this.bList)
      })
    })
  }
  getComments(id) {
    console.log("iddddddddddd",id);
    this.route.params.subscribe(params => {
      this.addComments.getComments(params['id']).subscribe((res: any) => {
        this.commentList = res;

        console.log("List with comments", this.commentList);
        for (var i = 0; i < this.commentList.length;i++) {
          this.userList.push(this.commentList[i].userId);
          this.commentList[i].userName =this.userList[i].fullName;
        }
 

        console.log("userName===========", this.commentList)
     
        document.getElementById('comments').style.display = "block";
      })
    })
  }
  addComment() {
    var comment = (document.getElementById('comment') as HTMLInputElement).value
    var rating =this.val
    console.log("rating============",rating);
    this.route.params.subscribe(params=>{
      var data = {
        userid: this.userid,
        blogid: params.id,
        comment: comment,
        rating:rating
      }
      this.addComments.addComments(data).subscribe((res: any) => {
        console.log("Data", res);
      this.toastr.success('Successfully Added Comment');
        window.location.reload();
      })
    })
  }
}
