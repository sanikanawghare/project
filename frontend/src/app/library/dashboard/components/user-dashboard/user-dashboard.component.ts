import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ListProductService } from '../../../../services/list-product/list-product.service';
import { AddToCartService } from '../../../../services/addToCart/add-to-cart.service';
import { ToastrService } from 'ngx-toastr';
// import { CartComponent } from '../cart/cart.component';
@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {
  name:string;
  productList:any;
  cartDetails:any;
  userId:string;
  cartL:number;
  constructor(private toastr: ToastrService,private router:Router,private productlist:ListProductService,private cart:AddToCartService) { }

  ngOnInit() {
    this.name=(localStorage.getItem("username"));
  }
 
  logout() {
    localStorage.removeItem("token");
    localStorage.removeItem("username");
    localStorage.removeItem("_id");
    localStorage.removeItem("totalAmount");
    this.router.navigate(['/authentication/login']);
    this.toastr.success('Successfully logged out');       
  }
}
