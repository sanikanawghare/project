import { Component, OnInit, AfterViewInit, OnDestroy,ViewChild,ElementRef,ChangeDetectorRef } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl,NgForm, FormBuilder, Validators } from '@angular/forms';
import { ShippingService } from '../../../../services/shipping/shipping.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-shipping-details',
  templateUrl: './shipping-details.component.html',
  styleUrls: ['./shipping-details.component.css']
})
export class ShippingDetailsComponent implements OnInit, AfterViewInit, OnDestroy  {
  @ViewChild('cardInfo') cardInfo: ElementRef;
  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;
  userid;
  amt;
  userId:any;
  addShippingForm:FormGroup
  constructor(private toastr: ToastrService,private order:ShippingService,private fb:FormBuilder,private cd: ChangeDetectorRef,private addShippingDetails:ShippingService,private router:Router) { }

  ngAfterViewInit() {
    this.card = elements.create('card');
    this.card.mount(this.cardInfo.nativeElement);

    this.card.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }
  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }


  async onSubmit(form: NgForm) {
    const { token, error } = await stripe.createToken(this.card);

    if (error) {
      console.log('Something is wrong:', error);
    } else {
      console.log('Success!!!!!!!!!!!!!!!', token);
      // ...send the token to the your backend to process the charge
      var data={
        amount:this.amt,
        userId:this.userid,
        stripeToken:token.id,
      }
      this.addShippingDetails.paymentDetail(data)
      .subscribe((res:any)=>{
        // res=res.data;
        console.log("Data resp -------",res);
        if(res.message!=="Payment Successful"){
          this.toastr.success('Oops!Something went wrong, please try again'); 
        }else{
          this.orderSummary();
          this.toastr.success('Payment Successful'); 
        this.router.navigate(['/authentication/success/user/ordersummary']);
        }
      })
    }
  }

  ngOnInit() {
    this.userid = (localStorage.getItem("_id"));
    console.log("id========",this.userid);
    this.amt = Number((localStorage.getItem("totalAmount")));
    console.log("amt========",this.amt);

    console.log(this.amt);
    this.addShippingForm=new FormGroup({
      "fullName":new FormControl(''),
      "email":new FormControl(''),
      "contact":new FormControl(''),
      "addressLine1":new FormControl(''),
      "addressLine2":new FormControl(''),
      "city":new FormControl(''),
      "zipcode":new FormControl(''),
      "state":new FormControl(''),
      "country":new FormControl(''),
      "userId":new FormControl(this.userid),
      "totalAmount":new FormControl(this.amt)
    })
    this.addShippingForm= this.fb.group({  
      fullName:['',Validators.compose([Validators.required,Validators.maxLength(30),Validators.minLength(3)])],
      contact:['',Validators.compose([Validators.required,Validators.maxLength(10),Validators.minLength(10)])],   
      email:['',Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],   
      addressLine1:['',Validators.compose([Validators.required,Validators.maxLength(30),Validators.minLength(3)])],
      addressLine2:['',Validators.compose([Validators.required,Validators.maxLength(30),Validators.minLength(3)])] ,
      city:['',Validators.compose([Validators.required,Validators.maxLength(30),Validators.minLength(3)])],
      zipcode:['',Validators.compose([Validators.required,Validators.maxLength(6),Validators.minLength(6)])],
      state:['',Validators.compose([Validators.required,Validators.maxLength(30),Validators.minLength(3)])],
      country:['',Validators.compose([Validators.required,Validators.maxLength(30),Validators.minLength(3)])],
      userId:[this.userid],
      totalAmount:[this.amt]
    })
  }
  addShipping(){
    console.log("gdfgdf",this.addShippingForm.value);
   
      this.addShippingDetails.addShippingDetail(this.addShippingForm.value).subscribe((res:any)=>{
       console.log("response=====",res);
      })
      this.toastr.success('Successfully Added Shipping Details');   
    // document.getElementById('payment').style.display = "block";
  }
  openForm() {
    document.getElementById("payment").style.display = "block";
}
orderSummary(){
  this.userId=(localStorage.getItem('_id'));
  var userid=this.userId
  console.log("useriddddddddddd",userid);
  this.order.orderSummary({userid}).subscribe(res=>{
    console.log("Added to order Summary",res);
    // this.orderDetails=res;
  });
}
}
