import { Component, OnInit } from '@angular/core';
import { AddBlogService } from '../../../../../services/add-blog/add-blog.service';

@Component({
  selector: 'app-list-blogs',
  templateUrl: './list-blogs.component.html',
  styleUrls: ['./list-blogs.component.css']
})
export class ListBlogsComponent implements OnInit {
  blogList:any;
  constructor(private bloglist:AddBlogService,private deleteRec:AddBlogService) { }

  ngOnInit() {
    this.getList()
  }
  getList(){
    this.bloglist.blogList().subscribe(res=>{
      this.blogList=res;
      console.log("List",this.blogList)
    })
  }
  delete(id){
    this.deleteRec.deleteBlog(id).subscribe(res=>{
      console.log("deleted");
      this.getList();
    })
  }
}
