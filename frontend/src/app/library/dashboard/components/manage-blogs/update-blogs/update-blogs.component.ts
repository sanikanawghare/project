import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AddBlogService } from '../../../../../services/add-blog/add-blog.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-update-blogs',
  templateUrl: './update-blogs.component.html',
  styleUrls: ['./update-blogs.component.css']
})
export class UpdateBlogsComponent implements OnInit {
  updateBlogForm:FormGroup;
  bList:any;
  constructor(private toastr: ToastrService,private getBListId:AddBlogService,private fb:FormBuilder,private route:ActivatedRoute,private router:Router,private updateBlog:AddBlogService) { }

  @ViewChild('fileInput') fileInput: ElementRef;

  ngOnInit() {
    this.getBlogListId();
    this.updateBlogForm= this.fb.group({  
      blogTitle:new FormControl('',[Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])]),   
      des:new FormControl('',[Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])]),   
      blogImage:null
    })
  }
  getBlogListId(){
    this.route.params.subscribe(params=>{
      this.getBListId.getBlogListId(params['id']).subscribe((res:any)=>{
        console.log("blog list with is",res);
        this.updateBlogForm.patchValue({
          blogTitle:res.blogTitle,
          blogImage:res.blogImage,
          des:res.des
        })
      })
    })
  }
  onBlogImage(event) {
    if(event.target.files.length > 0) {
      let blogImage = event.target.files[0];
      this.updateBlogForm.get('blogImage').setValue(blogImage);
    }
  }
  private prepareSave(): any {
    let input = new FormData();
    input.append('blogTitle', this.updateBlogForm.get('blogTitle').value);
    input.append('des', this.updateBlogForm.get('des').value);
    input.append('blogImage', this.updateBlogForm.get('blogImage').value);
    console.log(input);
    return input;
  }  
  onUpdateBlog(){
    const formModel = this.prepareSave();
    console.log('formModel',formModel);
    this.route.params.subscribe(params=>{
      this.updateBlog.updateBlog(formModel,params['id']).subscribe((res:any)=>{
        res=res.data
      })
    })
    this.toastr.success('Successfully Updated Blog');
    this.router.navigate(['/authentication/success/admin/manageblogs/listblogs'])
  }
}
