import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AddBlogService } from '../../../../../services/add-blog/add-blog.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-blogs',
  templateUrl: './add-blogs.component.html',
  styleUrls: ['./add-blogs.component.css']
})
export class AddBlogsComponent implements OnInit {
  addBlogForm:FormGroup;

  constructor(private toastr: ToastrService,private messageService: MessageService,private fb:FormBuilder,private router:Router,private addBlog:AddBlogService) { }

  @ViewChild('fileInput') fileInput: ElementRef;

  ngOnInit() {
    this.addBlogForm= this.fb.group({  
      blogTitle:new FormControl('',[Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])]),   
      des:new FormControl('',[Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])]),   
      blogImage:null
    })
    this.addBlogForm= this.fb.group({  
      blogTitle:['',Validators.compose([Validators.required,Validators.maxLength(50),Validators.minLength(5)])],
      des:['',Validators.compose([Validators.required,Validators.maxLength(200),Validators.minLength(20)])],   
    })
  }
  onBlogImage(event) {
    if(event.target.files.length > 0) {
      let blogImage = event.target.files[0];
      this.addBlogForm.get('blogImage').setValue(blogImage);
    }
  }
  private prepareSave(): any {
    let input = new FormData();
    input.append('blogTitle', this.addBlogForm.get('blogTitle').value);
    input.append('des', this.addBlogForm.get('des').value);
    input.append('blogImage', this.addBlogForm.get('blogImage').value);
    console.log(input);
    return input;
  }
  onAddBlog(){
    const formModel = this.prepareSave();
    console.log('formModel',formModel)
    this.addBlog.addBlog(formModel)
    .subscribe((res:any)=>{
      res=res.data;
      this.toastr.success('Successfully Added Blog');
      console.log("Data",res);
      this.router.navigate(['/authentication/success/admin/manageblogs/listblogs'])
    })
  }
}
