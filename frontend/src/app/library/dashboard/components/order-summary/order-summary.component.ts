import { Component, OnInit } from '@angular/core';
import { ShippingService } from '../../../../services/shipping/shipping.service';
import { AddToCartService } from '../../../../services/addToCart/add-to-cart.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.css']
})
export class OrderSummaryComponent implements OnInit {
  orderDetails:any;
  productDetails:any;
  cartData=new Array;
  userId:string;
  constructor(private toastr: ToastrService,private order:ShippingService,private cart:AddToCartService,private route:ActivatedRoute) { }

  ngOnInit() {
    this.getOrderDetails();
    this.flushCart();
  }
  getOrderDetails(){
    this.userId=(localStorage.getItem('_id'));
    var userid=this.userId
    console.log("useriddddddddddd",userid);   
    this.order.listOrderSummary(userid).subscribe(res=>{
      console.log("listtttttttttttttttt",res);
      this.orderDetails=res;

    })
  }
  openForm() {
    document.getElementById("summary").style.display = "block";
}

flushCart(){
  console.log("User id===========",this.userId);
  this.cart.flushCart(this.userId).subscribe(res=>{
    console.log("deleted");
  })
}
}
