import { Component, OnInit } from '@angular/core';
import { AddToCartService } from '../../../../services/addToCart/add-to-cart.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartDetails:any;
  userId:string;
  cartLength:number;
  totalAmount:number
  totalAmt: number = 0;
  constructor(private toastr: ToastrService,private cart:AddToCartService,private route:ActivatedRoute) { }

  ngOnInit() {
    this.getCartDetails();
    
  }
  cartDetailList=new Array;
  getCartDetails(){
    this.userId=(localStorage.getItem('_id'));
    var userid=this.userId
    console.log("useriddddddddddd",userid);   
    this.cart.listCart(userid).subscribe(res=>{
      console.log("listtttttttttttttttt",res);
            for (var i = 0; i < res.length; i++) {
                this.totalAmt = Math.round(this.totalAmt + res[i].productRate)
            }
      this.totalAmount=this.totalAmt;
      this.cartDetails=res;
      var total=this.totalAmount;
      localStorage.setItem("totalAmount",total.toString());
      console.log("Amount==========",this.totalAmount)
    });
  }
  deleteItem(id){
      this.cart.deleteItem(id).subscribe(res=>{
        console.log("deleted");
        this.toastr.success('Successfully Deleted Product');
        this.getCartDetails();
      })
    }
    updateQuantity(item){
      this.userId=(localStorage.getItem('_id'));
      var userid=this.userId
      var quant= Number((document.getElementById('quant') as HTMLInputElement).value)
      console.log("quantity======",quant);
      var data={
        productid:item.productId._id,
        userid:userid,
        quantity:quant,
        rate:item.productRate
      }
      console.log("data",data)
          this.cart.updateQuantity(data).subscribe((res:any)=>{
            res=res.data
          })
         
        this.getCartDetails(); 
      }
  }
