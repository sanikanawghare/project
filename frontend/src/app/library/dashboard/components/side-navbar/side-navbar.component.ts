import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-side-navbar',
  templateUrl: './side-navbar.component.html',
  styleUrls: ['./side-navbar.component.css']
})
export class SideNavbarComponent implements OnInit {
name:string;
  constructor(private router:Router) { }

  ngOnInit() {
    this.name=(localStorage.getItem("username"));
  }
  logout() {
    localStorage.removeItem("token");
    localStorage.removeItem("username");
    this.router.navigate(['/authentication/login']);
    alert(" Logged out Success !");
      }
}
