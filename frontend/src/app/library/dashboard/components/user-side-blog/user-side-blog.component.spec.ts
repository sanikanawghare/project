import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSideBlogComponent } from './user-side-blog.component';

describe('UserSideBlogComponent', () => {
  let component: UserSideBlogComponent;
  let fixture: ComponentFixture<UserSideBlogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSideBlogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSideBlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
