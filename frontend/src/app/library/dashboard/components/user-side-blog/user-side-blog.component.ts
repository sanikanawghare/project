import { Component, OnInit } from '@angular/core';
import { AddBlogService } from '../../../../services/add-blog/add-blog.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommentsService } from '../../../../services/comments/comments.service';

@Component({
  selector: 'app-user-side-blog',
  templateUrl: './user-side-blog.component.html',
  styleUrls: ['./user-side-blog.component.css']
})
export class UserSideBlogComponent implements OnInit {
  blogList:any;
  commentList:any;
  addCommentForm:FormGroup;
  userid:string;
  constructor(private addComments:CommentsService,private bloglist:AddBlogService,private fb:FormBuilder,private router:Router) { }

  ngOnInit() {
    this.getList();
    this.addCommentForm= this.fb.group({  
      comment:new FormControl('',[Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])]),   
    })
    this.userid=(localStorage.getItem("_id"));
  }
  getList(){
    this.bloglist.blogList().subscribe(res=>{
      this.blogList=res;
      console.log("List",this.blogList)
    })
  }
  addComment(blog:any){
    var data={
      userid:this.userid,
      blogid:blog._id,
      comment:this.addCommentForm.value
    }
    this.addComments.addComments(data).subscribe((res:any)=>{
      res=res.data;
      console.log("Data",res);
    })
  }
}
