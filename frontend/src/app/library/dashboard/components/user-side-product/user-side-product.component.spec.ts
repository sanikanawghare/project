import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSideProductComponent } from './user-side-product.component';

describe('UserSideProductComponent', () => {
  let component: UserSideProductComponent;
  let fixture: ComponentFixture<UserSideProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSideProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSideProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
