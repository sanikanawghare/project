import { Component, OnInit } from '@angular/core';
import { ListProductService } from '../../../../services/list-product/list-product.service';
import { Router } from '@angular/router';
import { AddToCartService } from '../../../../services/addToCart/add-to-cart.service';
import { MessageService } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-side-product',
  templateUrl: './user-side-product.component.html',
  styleUrls: ['./user-side-product.component.css']
})
export class UserSideProductComponent implements OnInit {
  productList:any;
  userid:string;
  cartDetails:any;
  constructor(private toastr: ToastrService,private messageService: MessageService,private router:Router,private productlist:ListProductService,private cart:AddToCartService) { }

  ngOnInit() {
    this.getList();
    this.userid=(localStorage.getItem("_id"));
  }
  getList(){
    this.productlist.productList().subscribe(res=>{
      console.log("response======",res);
      this.productList=res;
      console.log("List",this.productList)
    })
  }
  addToCart(item:any){
    var data={
      userid:this.userid,
      productid:item._id,
      product_cost:item.productCost
    }
    console.log(data);
    this.cart.addToCart(data).subscribe(res=>{
      if(res.code==200){
        this.toastr.success('Successfully Added To Cart');
      }else{
        this.toastr.error('Oops! Please try again');
      }
    })
  } 
}
