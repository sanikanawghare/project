import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AddProductService } from '../../../../../services/add-product/add-product.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  addProductForm:FormGroup;

  @ViewChild('fileInput') fileInput: ElementRef;
  
  constructor(private toastr: ToastrService,private fb:FormBuilder,private addProd:AddProductService) { }

  ngOnInit() {

    this.addProductForm= this.fb.group({  
      productName:new FormControl('',[Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])]),   
      productCost:new FormControl('',[Validators.compose([Validators.required,Validators.maxLength(10),Validators.minLength(3)])]),   
      shippingCost:new FormControl('',[Validators.compose([Validators.required,Validators.maxLength(10),Validators.minLength(3)])]),   
      status:new FormControl('',[Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])]), 
      description:new FormControl('',[Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])]),         
      imagePath:null
    })

    this.addProductForm= this.fb.group({  
      productName:['',Validators.compose([Validators.required,Validators.maxLength(30),Validators.minLength(3)])],
      productCost:['',Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])],   
      status:['',Validators.compose([Validators.required,Validators.maxLength(30),Validators.minLength(3)])],
      description:['',Validators.compose([Validators.required,Validators.maxLength(30),Validators.minLength(3)])],
      email:['',Validators.required] 
    })
  }

  onFileChange(event) {
    if(event.target.files.length > 0) {
      let imagePath = event.target.files[0];
      this.addProductForm.get('imagePath').setValue(imagePath);
    }
  }
  private prepareSave(): any {
    let input = new FormData();
    input.append('productName', this.addProductForm.get('productName').value);
    console.log('name============', this.addProductForm.get('productName').value);
    input.append('productCost', this.addProductForm.get('productCost').value);
    console.log('pcost============', this.addProductForm.get('productCost').value);
    input.append('status', this.addProductForm.get('status').value);
    console.log('stat============', this.addProductForm.get('status').value);
    input.append('description', this.addProductForm.get('description').value);
    console.log('description============', this.addProductForm.get('description').value);
    input.append('imagePath', this.addProductForm.get('imagePath').value);
    console.log(input);
    return input;
  }
  onAdd(){
    const formModel = this.prepareSave();
    console.log('formModel',formModel)
    this.addProd.addProduct(formModel)
    .subscribe((res:any)=>{
      console.log("Data",res);
      this.toastr.success('Successfully Added Product');
    })
  }
}
