import { Component, OnInit } from '@angular/core';
import { ListProductService } from '../../../../../services/list-product/list-product.service';
import { Router } from '@angular/router';
import { DeleteProductService } from '../../../../../services/delete-product/delete-product.service';


@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {
  productList:any;
  constructor(private productlist:ListProductService,private router:Router,private deleteRec:DeleteProductService) { }

  ngOnInit() {
    this.getList()
  }
  getList(){
    this.productlist.productList().subscribe(res=>{
      this.productList=res;
      console.log("List",this.productList)
    })
  }
  delete(id){
    this.deleteRec.deleteProduct(id).subscribe(res=>{
      console.log("deleted");
      this.getList();
    })
    this.productlist.productList().subscribe(res=>{
      this.productList=res;
      console.log("List",this.productList);
    })
  }
}
