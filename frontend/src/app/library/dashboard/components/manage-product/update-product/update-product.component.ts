import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UpdateProductService } from '../../../../../services/update-product/update-product.service';
import { ListProductService } from '../../../../../services/list-product/list-product.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {
  updateProductForm:FormGroup
  root:Router
  productlist:any
  constructor(private toastr: ToastrService,private getPListId:ListProductService,private updateproduct:UpdateProductService,private fb:FormBuilder,private router:Router,private route:ActivatedRoute) { }


  @ViewChild('fileInput') fileInput: ElementRef;

  ngOnInit() {

    this.getProductListId();
    this.updateProductForm= this.fb.group({  
      productName:new FormControl('',[Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])]),   
      productCost:new FormControl('',[Validators.compose([Validators.required,Validators.maxLength(10),Validators.minLength(3)])]),   
      status:new FormControl('',[Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])]),
      description:new FormControl('',[Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])]),   
      imagePath:null
    })
  }

  getProductListId(){
    this.route.params.subscribe(params=>{
      this.getPListId.getProductListId(params['id']).subscribe((res:any)=>{
        console.log("product list with is",res);
        this.updateProductForm.patchValue({
          productName:res.productName,
          productCost:res.productCost,
          status:res.status,
          description:res.description
        })
      })
    })
  }

  onFileChange(event) {
    if(event.target.files.length > 0) {
      let imagePath = event.target.files[0];
      this.updateProductForm.get('imagePath').setValue(imagePath);
    }
  }

  private prepareSave(): any {
    let input = new FormData();
    input.append('productName', this.updateProductForm.get('productName').value);
    console.log('name============', this.updateProductForm.get('productName').value);
    input.append('productCost', this.updateProductForm.get('productCost').value);
    console.log('pcost============', this.updateProductForm.get('productCost').value);
    input.append('status', this.updateProductForm.get('status').value);
    console.log('stat============', this.updateProductForm.get('status').value);
    input.append('description', this.updateProductForm.get('description').value);
    console.log('description============', this.updateProductForm.get('description').value);
    input.append('imagePath', this.updateProductForm.get('imagePath').value);
    console.log(input);
    return input;
  }

  onUpdate(){
    const formModel = this.prepareSave();
    console.log('formModel',formModel);
    this.route.params.subscribe(params=>{
      this.updateproduct.updateProduct(formModel,params['id']).subscribe((res:any)=>{
        res=res.data
      })
    })
    this.toastr.success('Successfully Updated Product');
    this.router.navigate(['/authentication/success/admin/manageproduct/listproduct'])
  }

}
