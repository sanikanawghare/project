import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AddUserService } from '../../../../../services/addUser/add-user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  sForm:FormGroup;
  constructor(private toastr: ToastrService,private fb:FormBuilder,private sUp:AddUserService,private router:Router) { }

  ngOnInit() {
    this.sForm=new FormGroup({
      "newfname":new FormControl(''),
      "newuName":new FormControl(''),
      "newemail":new FormControl(''),
      "newpsword":new FormControl(''),
      "newrole":new FormControl(''),
      "newisDeactive":new FormControl('')
    
    })
    this.sForm= this.fb.group({  
      newfname:['',Validators.compose([Validators.required,Validators.maxLength(30),Validators.minLength(3)])],
      newuName:['',Validators.compose([Validators.required,Validators.maxLength(15),Validators.minLength(3)])],    
      newpsword:['',Validators.required], 
      newemail:['',Validators.required] ,
      newrole:['',Validators.required],
      newisDeactive:['',Validators.required],
    })
  }
  onAddUser(){
    console.log(" Me ithe ahe");

    this.sUp.addUserByAdmin(this.sForm.value)
    .subscribe((res:any)=>{
      console.log("Data resp -------",res);
    this.toastr.success('Successfully Added User');
    })
  }
}
