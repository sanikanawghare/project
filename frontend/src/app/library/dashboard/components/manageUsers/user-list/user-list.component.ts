import { Component, OnInit } from '@angular/core';
import { ListUserService } from '../../../../../services/list-user/list-user.service';
import { Router } from '@angular/router';
import { DeleteUserService } from '../../../../../services/delete-user/delete-user.service';
import { DeactivateService } from '../../../../../services/deactivate/deactivate.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  userList:any;
  constructor(private userlist:ListUserService,private deactivateUser:DeactivateService,private router:Router,private deleteRec:DeleteUserService) { }

  ngOnInit() {
    this.getList()
  }
  getList(){
    this.userlist.userList().subscribe(res=>{
      this.userList=res;
      console.log("List",this.userList)
    })
  }
  delete(id){
    this.deleteRec.deleteUser(id).subscribe(res=>{
      console.log("deleted");
      this.getList();
    })
  }
  deactivate(id){
    this.deactivateUser.deactivateUser(id).subscribe(res=>{
      console.log("deactivated");
      this.getList();
    })
  }
}
