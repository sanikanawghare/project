import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UpdateUserService } from '../../../../../services/update-user/update-user.service';
import { ListUserService } from '../../../../../services/list-user/list-user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {
  updateUserForm:FormGroup
  root:Router
  userlist:any;
  
  constructor(private toastr: ToastrService,private getUListId:ListUserService,private updateproduct:UpdateUserService,private router:Router,private route:ActivatedRoute) { }

  ngOnInit() {
   
    this.getUserListId();
    this.updateUserForm=new FormGroup({
      "newUsername":new FormControl(''),
      "newPassword":new FormControl(''),
      "newFullName":new FormControl(''),
      "newEmail":new FormControl(''),
      "newRole":new FormControl(''),
      "newisDeactive":new FormControl(''),
    })
  }


  getUserListId(){
    this.route.params.subscribe(params=>{
      this.getUListId.getUserListId(params['id']).subscribe((res:any)=>{
        console.log("product list with is",res);
        this.updateUserForm.patchValue({
          newUsername:res.username,
          newPassword:res.password,
          newFullName:res.fullName,
          newEmail:res.email,
          newRole:res.role,
          newisDeactive:res.isDeactive
        })
      })
    })
  }
  onUpdate(){
    console.log("update details====",this.updateUserForm.value);

    this.route.params.subscribe(params=>{
      this.updateproduct.updateUser(this.updateUserForm.value,params['id']).subscribe((res:any)=>{
        res=res.data
      })
    })
    this.toastr.success('Successfully Updated User');
    this.router.navigate(['/authentication/success/admin/manageusers/userlist'])
  }

}
