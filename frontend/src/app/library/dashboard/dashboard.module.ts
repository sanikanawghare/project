import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { UserDashboardComponent } from './components/user-dashboard/user-dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { UserListComponent } from './components/manageUsers/user-list/user-list.component';
import { SideNavbarComponent } from './components/side-navbar/side-navbar.component';
import { AddProductComponent } from './components/manage-product/add-product/add-product.component';
import { ListProductComponent } from './components/manage-product/list-product/list-product.component';
import { UpdateProductComponent } from './components/manage-product/update-product/update-product.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateUserComponent } from './components/manageUsers/update-user/update-user.component';
import { AddUserComponent } from './components/manageUsers/add-user/add-user.component';
import { AddBlogsComponent } from './components/manage-blogs/add-blogs/add-blogs.component';
import { ListBlogsComponent } from './components/manage-blogs/list-blogs/list-blogs.component';
import { UpdateBlogsComponent } from './components/manage-blogs/update-blogs/update-blogs.component';
import { UserSideBlogComponent } from './components/user-side-blog/user-side-blog.component';
import { UserSideProductComponent } from './components/user-side-product/user-side-product.component';
import {AccordionModule} from 'primeng/accordion'; 
import {EditorModule} from 'primeng/editor';
import { CartComponent } from './components/cart/cart.component';
import {DropdownModule} from 'primeng/dropdown';
import {MenuItem} from 'primeng/api';              
// import { BrowserModule } from '@angular/platform-browser';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CardModule} from 'primeng/card';
// import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { BlogDetailsComponent } from './components/blog-details/blog-details.component';
import {RatingModule} from 'primeng/rating';
import {SpinnerModule} from 'primeng/spinner';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import { ShippingDetailsComponent } from './components/shipping-details/shipping-details.component';
import { AdminDetailsComponent } from './components/admin-details/admin-details.component';
import { OrderSummaryComponent } from './components/order-summary/order-summary.component';
import {ToastModule} from 'primeng/toast';
import {MessageService} from 'primeng/api';
import { GlobalFooterComponent } from '../shared/components/global-footer/global-footer.component';
import {ChartModule} from 'primeng/chart';



@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    AccordionModule,
    DropdownModule,
    EditorModule,
    RatingModule,
    CardModule,
    SpinnerModule,
    ToastModule,
    ChartModule,
    ScrollPanelModule,

    
   
    // FroalaEditorModule.forRoot(), FroalaViewModule.forRoot()
  ],
  providers: [MessageService],
  declarations: [AdminDashboardComponent, UserDashboardComponent, UserListComponent, SideNavbarComponent, AddProductComponent, ListProductComponent, UpdateProductComponent, UpdateUserComponent, AddUserComponent, AddBlogsComponent, ListBlogsComponent, UpdateBlogsComponent, UserSideBlogComponent, UserSideProductComponent, CartComponent, BlogDetailsComponent, ShippingDetailsComponent, AdminDetailsComponent, OrderSummaryComponent,GlobalFooterComponent]
})
export class DashboardModule { }
