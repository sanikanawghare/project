import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes,RouterModule } from '@angular/router';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { UserDashboardComponent } from './components/user-dashboard/user-dashboard.component';
import { UserListComponent } from './components/manageUsers/user-list/user-list.component';
import { AddProductComponent } from './components/manage-product/add-product/add-product.component';
import { ListProductComponent } from './components/manage-product/list-product/list-product.component';
import { UpdateProductComponent } from './components/manage-product/update-product/update-product.component';
import { UpdateUserComponent } from './components/manageUsers/update-user/update-user.component';
import { AddUserComponent } from './components/manageUsers/add-user/add-user.component';
import { AddBlogsComponent } from './components/manage-blogs/add-blogs/add-blogs.component';
import { ListBlogsComponent } from './components/manage-blogs/list-blogs/list-blogs.component';
import { UpdateBlogsComponent } from './components/manage-blogs/update-blogs/update-blogs.component';
import { UserSideBlogComponent } from './components/user-side-blog/user-side-blog.component';
import { UserSideProductComponent } from './components/user-side-product/user-side-product.component';
import { CartComponent } from './components/cart/cart.component';
import { BlogDetailsComponent } from './components/blog-details/blog-details.component';
import { ShippingDetailsComponent } from './components/shipping-details/shipping-details.component';
import { AdminDetailsComponent } from './components/admin-details/admin-details.component';
import { OrderSummaryComponent } from './components/order-summary/order-summary.component';
import { AuthGuardService } from '../../services/auth-guard/auth-guard.service';
import { RoleGuardService } from '../../services/roleguard/role-guard.service';
import { AdminGuardService } from '../../services/adminGuard/admin-guard.service';

const routes :Routes =[
  {
    path:'admin',
    component:AdminDashboardComponent,
    canActivate:[AdminGuardService],
    children:[
      {
        path:'',
        component:AdminDetailsComponent
      },
      {
        path:'manageusers',
        children:[
          {
            path:'adduser',
            component:AddUserComponent
          },
          {
            path:'userlist',
            component:UserListComponent
          },
          {
            path:'updateuser/:id',
            component:UpdateUserComponent
          }
        ]
      },
      {
        path:'manageproduct',
        children:[
          {
            path:'listproduct',
            component:ListProductComponent
          },
          {
            path:'addproduct',
            component:AddProductComponent

          },
          {
            path:'updateproduct/:id',
            component:UpdateProductComponent
          }
        ]
      },
      {
        path:'manageblogs',
        children:[
          {
            path:'listblogs',
            component:ListBlogsComponent
          },
          {
            path:'addblogs',
            component:AddBlogsComponent
          },
          {
            path:'updateblogs/:id',
            component:UpdateBlogsComponent
          }
        ]
      }
    ]
  },
  {
    path:'user',
    component:UserDashboardComponent,
    canActivate:[RoleGuardService],
    children:[
      {
        path:'blogs',
        component:UserSideBlogComponent,
      },
      {
        path:'blogdetails/:id',
        component:BlogDetailsComponent
      },
      {
        path:'perfumes',
        component:UserSideProductComponent
      },
      {
        path:'cart',
        component:CartComponent
      },
      {
        path:'shippingdetails',
        component:ShippingDetailsComponent
      },
      {
        path:'ordersummary',
        component:OrderSummaryComponent
      }
    ]
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class DashboardRoutingModule { }
