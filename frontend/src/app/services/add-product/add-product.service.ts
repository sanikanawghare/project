import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddProductService {
  api_Url=environment.url;
  constructor(private http:HttpClient) { }
  addProduct(data:any):Observable<any>{
    console.log("dataaaa",data)
    return this.http.post(this.api_Url+"addProduct",data);
  }
}
