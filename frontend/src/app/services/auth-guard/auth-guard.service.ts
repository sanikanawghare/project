import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { MessageService } from 'primeng/api';
// import { ToastsManager } from 'ng2-toastr/ng2-toastr';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{
  canActivate() {
    console.log('AuthGuard#canActivate called');
    if (localStorage.getItem("token") != null) {
      // alert(" Authentication Success");
      return true
    }else{
      // alert(" Authentication Fail");
      return false
    }
  }
  constructor() { }
}
