import { Injectable } from '@angular/core';
import {HttpInterceptor,HttpRequest,HttpHandler} from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {
  // loginToken:any;
  constructor() { }
  loginToken=localStorage.getItem('token')
  intercept(req: HttpRequest<any>, next: HttpHandler) {

  const authRequest = req.clone({
    headers: req.headers.set("authorization", "Bearer " + this.loginToken)
  });
  console.log("ygtrygtry",authRequest)
  return next.handle(authRequest);
}
}
