import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddBlogService {
  api_Url=environment.url;
  constructor(private http:HttpClient) { }
  addBlog(data:any):Observable<any>{
    console.log("dataaaa",data)
    return this.http.post(this.api_Url+"addBlog",data);
  }
  blogList():Observable<any>{
    return this.http.get(this.api_Url+'blogList')
  }
  updateBlog(data,id):Observable<any>{
    console.log("Id is",id);
    return this.http.put(this.api_Url+'updateBlog?id='+id,data);
  }
  deleteBlog(id):Observable<any>{
    console.log("Id is",id);
    return this.http.delete(this.api_Url+'deleteBlog?id='+id);
  }
  getBlogListId(id:any):Observable<any>{
    return this.http.get(this.api_Url+'getBlogListId?id='+id)
  }
}
