import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListProductService {
  api_Url=environment.url;
  constructor(private http:HttpClient) { }
  productList():Observable<any>{
    return this.http.get(this.api_Url + 'productList')
  }
  getProductListId(id:any):Observable<any>{
    return this.http.get(this.api_Url+'getProductListId?id='+id)
  }
}
