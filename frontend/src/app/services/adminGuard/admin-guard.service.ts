import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AdminGuardService implements CanActivate{
  canActivate() {
    console.log('AuthGuard#canActivate called');
    if (localStorage.getItem("role") == 'admin') {
      // alert(" Authentication Success");
      return true
    }else{
      alert(" Authentication Fail");
      return false
    }
  }
  constructor() { }
}
