import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListUserService {
  api_Url=environment.url;
  constructor(private http:HttpClient) { }
  userList():Observable<any>{
    return this.http.get(this.api_Url+'userList')
  }
  getUserListId(id:any):Observable<any>{
    console.log("here===========");
    return this.http.get(this.api_Url+'getUserListId?id='+id)
  }
}
