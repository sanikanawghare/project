import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UpdateProductService {
  api_Url=environment.url;
  constructor(private http:HttpClient) { }
  updateProduct(data,id):Observable<any>{
    console.log("Id is",id);
    return this.http.put(this.api_Url+'updateProduct?id='+id,data);
  }
}
