import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CommentsService {
  api_Url=environment.url;
  constructor(private http:HttpClient) { }
  addComments(data:any):Observable<any>{
    console.log("dataaaa",data)
    return this.http.post(this.api_Url+"addComments",data);
  }
  getComments(userid:any):Observable<any>{
    return this.http.get(this.api_Url+"getComments?id="+userid);
  }
}
