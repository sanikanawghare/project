import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate{
  canActivate() {
    console.log('AuthGuard#canActivate called');
    if (localStorage.getItem("role") == 'user') {
      return true
    }else{
      alert("Authentication Fail")
      return false
    }
  }
  constructor() { }
}
