import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  api_Url=environment.url;
  constructor(private http:HttpClient) { }
  signUp(data:any):Observable<any>{
    console.log("dataaaa",data)
    return this.http.post(this.api_Url+"addUser",data);
  }
  login(data:any):Observable<any>{
    console.log("dataaaa",data)
    return this.http.post(this.api_Url+"login",data);
  }
}
