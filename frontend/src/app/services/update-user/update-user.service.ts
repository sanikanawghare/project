import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UpdateUserService {
  api_Url=environment.url;
  constructor(private http:HttpClient) { }
  updateUser(data,id):Observable<any>{
    console.log("Id is",id);
    return this.http.put(this.api_Url+'updateUser?id='+id,data);
  }
}
