import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeleteProductService {
  api_Url=environment.url;
  constructor(private http:HttpClient) { }
  deleteProduct(id):Observable<any>{
    console.log("Id is",id);
    return this.http.delete(this.api_Url+'deleteProduct?id='+id);
  }
}
