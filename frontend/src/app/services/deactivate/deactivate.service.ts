import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeactivateService {
  api_Url=environment.url;
  constructor(private http:HttpClient) { }
  deactivateUser(id):Observable<any>{
    console.log("Id is",id);
    return this.http.delete(this.api_Url+'deactivateUser?id='+id);
  }
}
