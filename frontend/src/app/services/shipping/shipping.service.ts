import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ShippingService {
  api_Url=environment.url;
 
  constructor(private http:HttpClient) { }

  addShippingDetail(data:any):Observable<any>{
    console.log("dataaaa",data)
    return this.http.post(this.api_Url+"addShippingDetails",data);
  }
  paymentDetail(data:any):Observable<any>{
    console.log("token===========",data);
    return this.http.post(this.api_Url+"payment",data);
  }
  orderSummary(userid:any):Observable<any>{
    console.log("userid===========",userid);
    return this.http.post(this.api_Url+"orderSummary",userid);
  }

  listOrderSummary(userid:any):Observable<any>{
    return this.http.get(this.api_Url+"listOrderSummary?userid="+userid)
  }
}
