import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddToCartService {
  api_Url=environment.url;
  constructor(private http:HttpClient) { }
  addToCart(order:any):Observable<any>{
    return this.http.post(this.api_Url+"addToCart",order)
  }
  listCart(userid:any):Observable<any>{
    return this.http.get(this.api_Url+"listCart?userid="+userid)
  }
  deleteItem(id):Observable<any>{
    console.log("Id is",id);
    return this.http.delete(this.api_Url+'deleteItem?id='+id);
  }
  updateQuantity(data):Observable<any>{
    return this.http.put(this.api_Url+'updateQuantity',data);
  }
  flushCart(id:any):Observable<any>{
    console.log("Id is",id);
    return this.http.delete(this.api_Url+'flushCart?id='+id);
  }
}
