import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddUserService {
  api_Url=environment.url;
  constructor(private http:HttpClient) { }
  addUserByAdmin(data:any):Observable<any>{
    console.log("dataaaa",data)
    return this.http.post(this.api_Url+"addUserByAdmin",data);
  }
}
