import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { UserListComponent } from './components/user-list/user-list.component';
import { AdminRoutingModule } from './admin-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule
  ],
  declarations: []
})
export class AdminModule { }
