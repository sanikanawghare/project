import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from 'src/app/library/shared/components/home/home.component';
import { AuthGuardService } from 'src/app/services/auth-guard/auth-guard.service';
import { RoleGuardService } from './services/roleguard/role-guard.service';
import { AboutComponent } from './library/shared/components/about/about.component';
import { ContactUsComponent } from './library/shared/components/contact-us/contact-us.component';


const routes :Routes =[
  {
    path:'',
    component : HomeComponent
  },
  {
    path:'about',
    component : AboutComponent
  },
  {
    path:'contact',
    component : ContactUsComponent
  },
  {
    path:'authentication',
    children:[
      {
        path:'',
        loadChildren:'./library/auth/auth.module#AuthModule',
        
      },
      {
        path:'success',
        loadChildren:'./library/dashboard/dashboard.module#DashboardModule',
        canActivate:[AuthGuardService]
      }
    ]
  },
]

@NgModule({
  imports: [
    CommonModule,
    [RouterModule.forRoot(routes)]
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
